const int buzzerPin = 13;

const int rewindButtonPin = 7;
const int playButtonPin = 8;
const int pauseButtonPin = 9;
const int stopButtonPin = 10;
const int fastForwardButtonPin = 11;

const int gameOfThronesNotes[] = {
	392, 262,

	311, 349, 392, 262,
	311, 349, 392, 262,
	311, 349, 392, 262,

	311, 349, 392, 262,
	311, 349, 392, 262,
	311, 349, 392, 262,
	311, 349, 392, 262,
	311, 349, 

	294, 196,

	262, 294, 311, 196,
	233, 262, 294, 196,
	233, 262, 294, 196,
	233, 262, 294, 196,
	233, 262, 294, 196,
	233, 262,

	294, 196,

	220, 247, 277, 196,
	233, 262, 294, 196,
	233, 262, 294, 196,
	233, 262, 294, 196,
	233, 262, 294, 196,
	233, 262,

	494, 370,

	392, 440, 494, 370,
	392, 440, 494, 370,
	392, 440, 494, 370,
	392, 440, 494, 370,
	392, 440, 494, 370,
	392, 440,

	494, 349,

	370, 392, 370, 330, 362,

	311, 349, 392, 262,
	311, 349, 392, 262,
	311, 349, 392, 262,
	311, 349, 392, 262,
	311, 349,

	196
};

const int gameOfThronesDelays[] = {
	40, 40,

	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,

	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 

	20, 20,

	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	80, 80,

	20, 20,

	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	80, 80,

	20,20,

	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	80, 80,

	20, 20,

	20, 20, 20, 20, 20,

	70, 70, 40, 40,
	70, 70, 40, 40,
	70, 70, 40, 40,
	20, 20, 

	10
};

const int gameOfThronesTonesLength = 130;	// = sizeof(gameOfThronesNotes) / sizeof(gameOfThronesNotes[0]);

// const int gameOfThronesDelaysLength = 130; // = sizeof(gameOfThronesDelays) / sizeof(gameOfThronesDelays[0]);

void setup() {
	pinMode(buzzerPin, OUTPUT);

	pinMode(rewindButtonPin, INPUT);
	pinMode(playButtonPin, INPUT);
	pinMode(pauseButtonPin, INPUT);
	pinMode(stopButtonPin, INPUT);
	pinMode(fastForwardButtonPin, INPUT);
}

void loop() {
	boolean rewindKeyUp = digitalRead(rewindButtonPin);
	boolean playKeyUp = digitalRead(playButtonPin);
	boolean pauseKeyUp = digitalRead(pauseButtonPin);
	boolean stopKeyUp = digitalRead(stopButtonPin);
	boolean fastForwardKeyUp = digitalRead(fastForwardButtonPin);

	int action = 0;				// 0-nothing, 1-rewind, 2-play, 3-pause, 4-stop, 5-fast-forward
	long jumpTimeCounter = 0;
	int jumDifference = 0;

	int currentToneSavedPosition = 0;

	if(!rewindKeyUp) {	
		jumpTimeCounter -= millis();
		action = 1;
	}

	if(!playKeyUp) {
		if(jumpTimeCounter != 0) {
			jumDifference = getDistanceLengthForJumping(jumpTimeCounter);
		}
		jumpTimeCounter = 0;
		action = 2;
	}

	if(!pauseKeyUp) {
		jumDifference= 0;
		jumpTimeCounter = 0;
		action = 3;
	}

	if(!stopKeyUp) {
		if(jumpTimeCounter != 0) {
			jumDifference = getDistanceLengthForJumping(jumpTimeCounter);
		}
		jumDifference = 0;
		jumpTimeCounter = 0;
		action = 4;
	}

	if(!fastForwardKeyUp) {
		jumpTimeCounter += millis();
		action = 5;
	}

	int startPosition = getStartPositionForMelody(currentToneSavedPosition, jumDifference);

	playSounds(action, startPosition, currentToneSavedPosition);
}


void playSounds(int action, int startPosition, int &toneSavedPosition) {
	int beginning = 0 + startPosition;

	for(int currTone = beginning; currTone < gameOfThronesTonesLength; ++currTone) {

	// 0-nothing, 1-rewind, 2-play, 3-pause, 4-stop, 5-fast-forward		

		if(action == 1 || action == 5) {
			noTone(buzzerPin);
		}
		if(action == 4) {
			noTone(buzzerPin);
			toneSavedPosition = 0;
		}
		if(action == 3) {
			noTone(buzzerPin);
			toneSavedPosition = currTone;
		}
		if(action == 2) {
			int currentToneDuration = 1700 / gameOfThronesDelays[currTone];
			tone(buzzerPin, gameOfThronesNotes[currTone], currentToneDuration);

			int pauseBetweenTones = currentToneDuration * 1.2;	//tone's duration with 20%
			delay(pauseBetweenTones);

			toneSavedPosition = currTone;

			noTone(buzzerPin);
		}
	}
}


int getDistanceLengthForJumping(long jump) {
	// how it is evaluated:
	// 1) we get three-digit-remainder after evaluating difference between current and freezed time values
	// 2) we get special number < 100 after division the three-digit-remainder to 10
	long difference = ((millis() - abs(jump)) % 1000) / 10;	//where 1000 == 1 sec
	return jump > 0 ? difference : 0 - difference;
}


int getStartPositionForMelody(int savedTonePosition, int jump) {
	int result = savedTonePosition + jump;
	if(result < 0) {
		return 0;
	} else if(result >= gameOfThronesTonesLength) {
		return gameOfThronesTonesLength - 2;
	} else {
		return result;
	}
}